﻿import sys
# fix unicode import error for matplotlib
reload(sys)
sys.setdefaultencoding('utf8')

import numpy as np
import matplotlib.pyplot as plt
import time
from threading import Thread
import tobii_research as tr

valFocusedPoints = []
def gaze_data_callback(gaze_data):
    gaze_left_eye = gaze_data['left_gaze_point_on_display_area']
    gaze_right_eye = gaze_data['right_gaze_point_on_display_area']
    global valFocusedPoints
    # no nan
    if not (is_nan(gaze_left_eye[0]) or is_nan(gaze_left_eye[1]) or is_nan(gaze_right_eye[0]) or is_nan(gaze_right_eye[1])):
        valFocusedPoints.append(((gaze_left_eye[0] + gaze_right_eye[0]) * 0.5, (gaze_left_eye[1] + gaze_right_eye[1]) * 0.5))

def is_nan(x):
    return (x is np.nan or x != x)

class EyeTrackerCalibration():
    def __init__(self):
        self.ready = False
        self.nextPointShown = False
        self.nextPointScanned = False
        # time to focus in seconds
        self.waitingTime = 2
        # automatically
        self.screen_width = 1920
        self.screen_height = 1080
        
        self.dotSize = 10
        found_eyetrackers = tr.find_all_eyetrackers()
        self.eyetracker = found_eyetrackers[0]
        self.initPlot()
        
    def initPlot(self):
        # Define the points on screen we should calibrate at.
        # The coordinates are normalized, i.e. (0.0, 0.0) is the upper left corner and (1.0, 1.0) is the lower right corner.
        self.points_to_calibrate = [(0.5, 0.5), (0.1, 0.1), (0.1, 0.9), (0.9, 0.9), (0.9, 0.1)]
        self.points_to_calibrate_plt = [(self.screen_width / 2, self.screen_height / 2), (0, self.screen_height), (0,0), (self.screen_width,0), (self.screen_width, self.screen_height)]
        fig = plt.gcf()
        DPI = fig.get_dpi()
        fig.set_size_inches(self.screen_width/float(DPI), self.screen_height/float(DPI))
        plt.subplots_adjust(left=0, right=1, top=1, bottom=0)

        #disable axis, labels and toolbar
        plt.ion()
        plt.rcParams['toolbar'] = 'None'
        plt.axis('off')

        mng = plt.get_current_fig_manager()
        mng.window.state('zoomed')
                
        for p in self.points_to_calibrate_plt:
            plt.plot(p[0], p[1], marker='o', markersize=self.dotSize, color="white")

    # function that performs calibration of the device, draws 5 points and collects data
    def calibrateEyeTrackerThread(self):
        calibration = tr.ScreenBasedCalibration(self.eyetracker)

        # Enter calibration mode.
        calibration.enter_calibration_mode()
        print "Entered calibration mode for eye tracker with serial number {0}.".format(self.eyetracker.serial_number)
 
        self.ready = True
        for point in self.points_to_calibrate:
            # wait until point is shown
            while not self.nextPointShown:
                pass
                
            time.sleep(self.waitingTime)
            # Wait a little for user to focus.
            self.nextPointScanned = False
            print "Collecting data at {0}.".format(point)
            
            calibration.collect_data(point[0], point[1])
            self.nextPointScanned = True
           
        # Compute and apply again.
        print "Computing and applying calibration."
        calibration_result = calibration.compute_and_apply()
        print "Compute and apply returned {0} and collected at {1} points.".\
        format(calibration_result.status, len(calibration_result.calibration_points))

        # The calibration is done. Leave calibration mode.
        calibration.leave_calibration_mode()
        print "Left calibration mode."

    def calibrate(self):
        # prepare the plot
        plt.show()

        # some variables for synchronization
        self.nextPointScanned = False
        self.nextPointShown = False
        self.ready = False

        thread1 = Thread(target = self.calibrateEyeTrackerThread)
        thread1.start()
        print "Thread started"

        # this is needed for synchronization with eye tracking thread
        # initializing the device takes some time...
        while not self.ready:
            pass
        for p in self.points_to_calibrate_plt:
            self.nextPointShown = False
            plt.plot(p[0], p[1], marker='o', markersize=self.dotSize, color="red")
            self.nextPointShown = True
            plt.pause(self.waitingTime)
            
            while not self.nextPointScanned:
                pass
            # clear 
            plt.plot(p[0], p[1], marker='o', markersize=self.dotSize+10, color="white")
        # wait for other thread to finish
        thread1.join()

    def validate_calibration(self):
        plt.show()
        global valFocusedPoints
        # validate calibration with test points!!
        test_points = [(self.screen_width / 2, 0), (self.screen_width, self.screen_height / 2), (0,self.screen_height)]
        test_points_rel = [(0.5,0.9), (0.9,0.5), (0.1,0.1)]
        test_focused_points = []
        for p, prel in zip(test_points, test_points_rel):
            nextPointShown = False
                
            plt.plot(p[0], p[1], marker='o', markersize=self.dotSize, color="blue")
            valFocusedPoints = []
            self.eyetracker.subscribe_to(tr.EYETRACKER_GAZE_DATA, gaze_data_callback, as_dictionary=True)
            plt.pause(self.waitingTime)
            self.eyetracker.unsubscribe_from(tr.EYETRACKER_GAZE_DATA, gaze_data_callback)
            #clear
            plt.plot(p[0], p[1], marker='o', markersize=self.dotSize+10, color="white")
            avgPoint = np.array(np.mean(valFocusedPoints, axis=0))
            print len(valFocusedPoints)
            # convert to plt coordinates
            test_focused_points.append((avgPoint[0] * self.screen_width, (1-avgPoint[1]) * self.screen_height))
            #test_focused_points.append(avgPoint)
            prel = np.array(prel)
            print np.linalg.norm(avgPoint - prel)
            
        # show result
        for p, pf in zip(test_points, test_focused_points):
            nextPointShown = False
            # delete other points
            plt.plot(p[0], p[1], marker='o', markersize=self.dotSize, color="blue")
            plt.plot(pf[0], pf[1], marker='o', markersize=self.dotSize/2, color="black")
        while True:
            plt.pause(self.waitingTime)
            
if __name__ == "__main__":
    calibration = EyeTrackerCalibration()
    calibration.calibrate()
    calibration.validate_calibration()