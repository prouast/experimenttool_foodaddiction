# pip install -U setuptools
# pip install tobii_research
import tobii_research as tr
import time
import sys

def gaze_data_callback(gaze_data):
    gaze_left_eye = gaze_data['left_gaze_point_on_display_area']
    gaze_right_eye = gaze_data['right_gaze_point_on_display_area']
    print str(gaze_left_eye[0]) + ";" + str(gaze_left_eye[1]) + ";" + str(gaze_right_eye[0]) + ";" + str(gaze_right_eye[1])
    #print gaze_data['device_time_stamp']
    #print gaze_data['system_time_stamp']
    sys.stdout.flush()

found_eyetrackers = tr.find_all_eyetrackers()
my_eyetracker = found_eyetrackers[0]
        
my_eyetracker.subscribe_to(tr.EYETRACKER_GAZE_DATA, gaze_data_callback, as_dictionary=True)

# if one argument given, we are in debug mode
if len(sys.argv) > 1:
    time.sleep(6)
else:
    while True:
        time.sleep(0.5)
        # check if we have to abort
        if len(sys.stdin.read()) > 0:
            break

my_eyetracker.unsubscribe_from(tr.EYETRACKER_GAZE_DATA, gaze_data_callback)
