package edu.kit.exp.sensor.eyeTracking;

import edu.kit.exp.common.Constants;
import edu.kit.exp.common.LogHandler;
import edu.kit.exp.common.RecordingException;
import edu.kit.exp.common.sensor.ISensorRecorder;

import java.io.*;
import java.util.Date;

public class EyeTrackingRecorder extends ISensorRecorder<EyeTrackingConfiguration> {

    private static final String MENU_TEXT = "EyeTracking Tobii Pro X3-120";
    private static final String PYTHON_SCRIPT_PATH = "EyeTracking.py";

    /** The python process. */
    private Process process;

    /** Readers and writers for python. */
    private BufferedReader inputReader;
    private BufferedReader errorReader;
    private BufferedWriter writer;
    private boolean errorReaderRunning;

    @Override
    public String getMenuText() {
        return MENU_TEXT;
    }

    @Override
    public boolean configureRecorder(EyeTrackingConfiguration configuration) {

        // Initialise buffered writer
        try {
            writer = new BufferedWriter(new FileWriter("eyetracking" + Constants.getComputername() + System.currentTimeMillis() + ".tsv", true));
            writer.write("timestamp;leftx;lefty;rightx;righty\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Initialise python process
        // TODO use ProcessBuilder
        // https://bytes.com/topic/python/insights/949995-three-ways-run-python-programs-java
        try {
            process = Runtime.getRuntime().exec(configuration.pythonCommand + " " + PYTHON_SCRIPT_PATH);
            inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Listen to errors in python script
        Runnable runnable = () -> {
            errorReaderRunning = true;
            String line;
            try {
                while (errorReaderRunning && (line = errorReader.readLine()) != null) {
                    LogHandler.printInfo("Error in python script: " + line.trim());
                    stopRecording();
                }
            } catch (IOException e) {
                LogHandler.printException(e, "Exception reading python script error messages.");
            }
        };
        new Thread(runnable).start();

        return true;
    }

    @Override
    public void Recording() throws RecordingException {

        String line;
        try {
            // wait for first input, device must first initialize itself
            while (inputReader.readLine() == null);

            // Main recording loop
            while (isCapturingActive() && (line = inputReader.readLine()) != null) {
                String data = line.trim();
                writer.write( new Date().getTime() + "\t" + data + "\n");
                writer.flush();
            }
        } catch (IOException e) {
            throw new RecordingException("Exception while recording eye tracking.");
        }
    }

    @Override
    public void cleanupRecorder() {
        // Stop reading errors
        errorReaderRunning = false;
        // Shut down python process
        PrintStream printStream = new PrintStream(process.getOutputStream());
        printStream.print("terminate");
        printStream.close();
        // Shut down writer
        try {
            LogHandler.printInfo("Closing file");
            writer.close();
        } catch (IOException e) {
            LogHandler.printException(e, "Exception while cleaning up eye tracking.");
        }
    }
}
