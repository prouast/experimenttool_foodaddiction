package edu.kit.exp.sensor.eyeTracking;

import edu.kit.exp.common.sensor.ISensorRecorderConfiguration;
import edu.kit.exp.common.sensor.SensorConfigurationElement;

public class EyeTrackingConfiguration extends ISensorRecorderConfiguration {

    /** serialVersionUID */
    private static final long serialVersionUID = -2291728016832895247L;

    @SensorConfigurationElement(name = "Python Command", description = "Name of python2 binary, should be in PATH")
    public String pythonCommand;

    @Override
    public void setDefaultValues() {
        pythonCommand = "python";
    }
}
