package edu.kit.exp.impl.foodaddiction.server;

import edu.kit.exp.server.jpa.entity.Period;
import edu.kit.exp.server.jpa.entity.Subject;
import edu.kit.exp.server.microeconomicsystem.RoleMatcher;
import edu.kit.exp.server.run.RandomGeneratorException;

import java.util.ArrayList;
import java.util.List;

/**
 * This class simply adds the single subject to the group, since it is a single participant experiment.
 */
public class FoodAddictionRoleMatcher extends RoleMatcher {

    public FoodAddictionRoleMatcher(List<String> roles) {
        super(roles);
    }

    @Override
    public List<Subject> rematch(Period period, List<Subject> subjects) throws RandomGeneratorException {
        List <Subject> subject = new ArrayList<Subject>();
        subject.add(subjects.get(0));
        return subjects;
    }

    @Override
    public List<Subject> setupSubjectRoles(List<Subject> subjects) throws RandomGeneratorException {
        List <Subject> subject = new ArrayList <Subject> ();
        subject.add(subjects.get(0));
        return subjects;
    }
}
