package edu.kit.exp.impl.foodaddiction.server;

public class FoodAddictionEnvironmentGroup1 extends FoodAddictionEnvironment {

    @Override
    public String[] getInterval1ImagePaths() {
        return HEALTHY_FOOD_PATHS;
    }

    @Override
    public String[] getInterval2ImagePaths() {
        return JUNK_FOOD_PATHS;
    }
}
