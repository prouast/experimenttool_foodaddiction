package edu.kit.exp.impl.foodaddiction.server;

public class FoodAddictionEnvironmentGroup2 extends FoodAddictionEnvironment {

    @Override
    public String[] getInterval1ImagePaths() {
        return JUNK_FOOD_PATHS;
    }

    @Override
    public String[] getInterval2ImagePaths() {
        return HEALTHY_FOOD_PATHS;
    }
}
