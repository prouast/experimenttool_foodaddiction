package edu.kit.exp.impl.foodaddiction.server;


import edu.kit.exp.common.LogHandler;
import edu.kit.exp.impl.foodaddiction.client.FoodAddictionImageScreen;
import edu.kit.exp.impl.foodaddiction.client.FoodAddictionInfoScreen;
import edu.kit.exp.impl.foodaddiction.client.FoodAddictionRestScreen;
import edu.kit.exp.impl.foodaddiction.client.FoodAddictionSteeringScreen;
import edu.kit.exp.server.communication.ClientResponseMessage;
import edu.kit.exp.server.communication.ServerMessageSender;
import edu.kit.exp.server.jpa.entity.Membership;
import edu.kit.exp.server.jpa.entity.Subject;
import edu.kit.exp.server.microeconomicsystem.Institution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * This class provides a microeconomic environment for a food addiction experiment.
 * This class defines the sequence of the experiment.
 */
public class FoodAddictionInstitution extends Institution<FoodAddictionEnvironment> {

    /** The participant. */
    private Subject participant;

    /**
     * Instantiates a new food addiction experiment institution.
     * @param environment The environment
     * @param memberships The memberships
     * @param messageSender The message sender
     * @param gameId The game id
     */
    public FoodAddictionInstitution(FoodAddictionEnvironment environment, List<Membership> memberships,
                                    ServerMessageSender messageSender, String gameId) {
        super(environment, memberships, messageSender, gameId);

        participant = memberships.get(0).getSubject();
    }

    @Override
    public void startPeriod() throws Exception {

        LogHandler.printInfo("Institution started");

        if (getCurrentPeriod().getPractice()) {

            // First, eye tracking device has to be calibrated (5 min)

            // Welcome and instructions screen
            showScreenWithDeadLine(
                    participant,
                    FoodAddictionInfoScreen.class,
                    new FoodAddictionInfoScreen.ParamObject(
                            FoodAddictionInfoScreen.LogValType.WELCOME_START,
                            new ArrayList<>(Collections.singletonList("Welcome to the experiment."))),
                    FoodAddictionEnvironment.INFO_TIME);

            // Info screen announcing start of trial
            showScreenWithDeadLine(
                    participant,
                    FoodAddictionInfoScreen.class,
                    new FoodAddictionInfoScreen.ParamObject(
                            FoodAddictionInfoScreen.LogValType.TRIAL_1_INFO_START,
                            new ArrayList<>(Collections.singletonList("Trial period 1 will start now."))),
                    FoodAddictionEnvironment.INFO_TIME);

            // First set of trial images
            showScreen(
                    participant,
                    FoodAddictionImageScreen.class,
                    new FoodAddictionImageScreen.ParamObject(
                            FoodAddictionImageScreen.LogValType.TRIAL_1,
                            FoodAddictionEnvironment.TRIAL_1_PATHS,
                            FoodAddictionEnvironment.NUMBER_OF_TRIAL_IMAGES,
                            FoodAddictionEnvironment.DISPLAY_TIME_TRIAL,
                            FoodAddictionEnvironment.FIXATION_TIME,
                            new ArrayList<>()));

        } else {

            // Info screen introducing rest phase
            showScreenWithDeadLine(
                    participant,
                    FoodAddictionInfoScreen.class,
                    new FoodAddictionInfoScreen.ParamObject(
                            FoodAddictionInfoScreen.LogValType.INITIAL_REST_INFO_START,
                            new ArrayList<>(Collections.singletonList("The 5-minute rest period is about to start."))),
                    FoodAddictionEnvironment.INFO_TIME);

            // Rest screen
            showScreenWithDeadLine(
                    participant,
                    FoodAddictionRestScreen.class,
                    new FoodAddictionRestScreen.ParamObject(),
                    FoodAddictionEnvironment.REST_TIME);

            // Info screen announcing end of rest phase
            showScreenWithDeadLine(
                    participant,
                    FoodAddictionInfoScreen.class,
                    new FoodAddictionInfoScreen.ParamObject(
                            FoodAddictionInfoScreen.LogValType.INITIAL_REST_INFO_END,
                            new ArrayList<>(Collections.singletonList("End of the rest period."))),
                    FoodAddictionEnvironment.INFO_TIME);

            // Info screen announcing end of rest phase
            showScreenWithDeadLine(
                    participant,
                    FoodAddictionInfoScreen.class,
                    new FoodAddictionInfoScreen.ParamObject(
                            FoodAddictionInfoScreen.LogValType.INTERVAL_1_INFO_START,
                            new ArrayList<>(Collections.singletonList("The experiment will start now."))),
                    FoodAddictionEnvironment.INFO_TIME);

            // First set of images
            showScreen(
                    participant,
                    FoodAddictionImageScreen.class,
                    new FoodAddictionImageScreen.ParamObject(
                            FoodAddictionImageScreen.LogValType.INTERVAL_1,
                            environment.getInterval1ImagePaths(),
                            FoodAddictionEnvironment.NUMBER_OF_FOOD_IMAGES,
                            FoodAddictionEnvironment.DISPLAY_TIME,
                            FoodAddictionEnvironment.FIXATION_TIME,
                            FoodAddictionEnvironment.NEXT_PROMPT_TIMES));
        }
    }

    @Override
    public void processMessage(ClientResponseMessage msg) throws Exception {

        LogHandler.printInfo("Processing message: " + msg);

        // Identify message sender
        String msgScreenId = msg.getScreenId();

        // Returning from image screen
        if (msgScreenId.equals(FoodAddictionImageScreen.class.getName())) {

            switch (((FoodAddictionImageScreen.ParamObject)msg.getParameters()).getLogVal()) {

                case TRIAL_1:
                    // Show info screen about trial steering task
                    showScreenWithDeadLine(
                            participant,
                            FoodAddictionInfoScreen.class,
                            new FoodAddictionInfoScreen.ParamObject(
                                    FoodAddictionInfoScreen.LogValType.STEERING_TRIAL_INFO_START,
                                    new ArrayList<>(Collections.singletonList("The steering task will start next."))),
                            FoodAddictionEnvironment.INFO_TIME);
                    // Show trial steering task
                    showScreen(
                            participant,
                            FoodAddictionSteeringScreen.class,
                            new FoodAddictionSteeringScreen.ParamObject(
                                    FoodAddictionSteeringScreen.LogValType.TRIAL,
                                    FoodAddictionEnvironment.w,
                                    FoodAddictionEnvironment.D));
                    break;

                case TRIAL_2:
                    endPeriod();
                    break;

                case INTERVAL_1:
                    // Show info screen about steering task 1
                    showScreenWithDeadLine(
                            participant,
                            FoodAddictionInfoScreen.class,
                            new FoodAddictionInfoScreen.ParamObject(
                                    FoodAddictionInfoScreen.LogValType.STEERING_1_INFO_START,
                                    new ArrayList<>(Collections.singletonList("The steering task will start next."))),
                                    FoodAddictionEnvironment.INFO_TIME);
                    // Show first steering task
                    showScreen(
                            participant,
                            FoodAddictionSteeringScreen.class,
                            new FoodAddictionSteeringScreen.ParamObject(
                                    FoodAddictionSteeringScreen.LogValType.INTERVAL_1,
                                    FoodAddictionEnvironment.w,
                                    FoodAddictionEnvironment.D));
                    break;

                case WASH_OUT:
                    // Show info screen announcing second interval
                    showScreenWithDeadLine(
                            participant,
                            FoodAddictionInfoScreen.class,
                            new FoodAddictionInfoScreen.ParamObject(
                                    FoodAddictionInfoScreen.LogValType.INTERVAL_2_INFO_START,
                                    new ArrayList<>(Collections.singletonList("The second interval will start now."))),
                            FoodAddictionEnvironment.INFO_TIME);
                    // Second set of images
                    showScreen(
                            participant,
                            FoodAddictionImageScreen.class,
                            new FoodAddictionImageScreen.ParamObject(
                                    FoodAddictionImageScreen.LogValType.INTERVAL_2,
                                    environment.getInterval2ImagePaths(),
                                    FoodAddictionEnvironment.NUMBER_OF_FOOD_IMAGES,
                                    FoodAddictionEnvironment.DISPLAY_TIME,
                                    FoodAddictionEnvironment.FIXATION_TIME,
                                    FoodAddictionEnvironment.NEXT_PROMPT_TIMES));
                    break;

                case INTERVAL_2:
                    // Show info screen about steering task 2
                    showScreenWithDeadLine(
                            participant,
                            FoodAddictionInfoScreen.class,
                            new FoodAddictionInfoScreen.ParamObject(
                                    FoodAddictionInfoScreen.LogValType.STEERING_2_INFO_START,
                                    new ArrayList<>(Collections.singletonList("The steering task will start next."))),
                            FoodAddictionEnvironment.INFO_TIME);
                    // Show first steering task
                    showScreen(
                            participant,
                            FoodAddictionSteeringScreen.class,
                            new FoodAddictionSteeringScreen.ParamObject(
                                    FoodAddictionSteeringScreen.LogValType.INTERVAL_2,
                                    FoodAddictionEnvironment.w,
                                    FoodAddictionEnvironment.D));
                    break;

                default:
                    break;
            }
        }

        // Returning from steering screen
        else if (msgScreenId.equals(FoodAddictionSteeringScreen.class.getName())) {

            switch (((FoodAddictionSteeringScreen.ParamObject)msg.getParameters()).getLogVal()) {

                case TRIAL:
                    // Show info screen about washout task
                    showScreenWithDeadLine(
                            participant,
                            FoodAddictionInfoScreen.class,
                            new FoodAddictionInfoScreen.ParamObject(
                                    FoodAddictionInfoScreen.LogValType.TRIAL_2_INFO_START,
                                    new ArrayList<>(Collections.singletonList("A series of further images will be shown now."))),
                            FoodAddictionEnvironment.INFO_TIME);
                    // Show wash out task
                    showScreen(
                            participant,
                            FoodAddictionImageScreen.class,
                            new FoodAddictionImageScreen.ParamObject(
                                    FoodAddictionImageScreen.LogValType.TRIAL_2,
                                    FoodAddictionEnvironment.TRIAL_2_PATHS,
                                    FoodAddictionEnvironment.NUMBER_OF_TRIAL_IMAGES,
                                    FoodAddictionEnvironment.DISPLAY_TIME_TRIAL,
                                    FoodAddictionEnvironment.FIXATION_TIME,
                                    new ArrayList<>()));
                    break;

                case INTERVAL_1:
                    // Show info screen about washout task
                    showScreenWithDeadLine(
                            participant,
                            FoodAddictionInfoScreen.class,
                            new FoodAddictionInfoScreen.ParamObject(
                                    FoodAddictionInfoScreen.LogValType.WASH_OUT_INFO_START,
                                    new ArrayList<>(Collections.singletonList("A series of further images will be shown now."))),
                                    FoodAddictionEnvironment.INFO_TIME);
                    // Show wash out task
                    showScreen(
                            participant,
                            FoodAddictionImageScreen.class,
                            new FoodAddictionImageScreen.ParamObject(
                                    FoodAddictionImageScreen.LogValType.WASH_OUT,
                                    FoodAddictionEnvironment.NEUTRAL_PATHS,
                                    FoodAddictionEnvironment.NUMBER_OF_NEUTRAL_IMAGES,
                                    FoodAddictionEnvironment.DISPLAY_TIME,
                                    FoodAddictionEnvironment.FIXATION_TIME,
                                    new ArrayList<>()));
                    break;

                case INTERVAL_2:
                    endPeriod();
                    break;

                default:
                    break;
            }
        }
    }

    @Override
    protected void endPeriod() throws Exception {
        this.finished = true;
    }
}
