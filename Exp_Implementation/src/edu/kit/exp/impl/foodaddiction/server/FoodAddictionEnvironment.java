package edu.kit.exp.impl.foodaddiction.server;

import edu.kit.exp.server.microeconomicsystem.Environment;
import edu.kit.exp.server.microeconomicsystem.RoleMatcher;
import edu.kit.exp.server.microeconomicsystem.SingleRoleGroupMatcher;
import edu.kit.exp.server.microeconomicsystem.SubjectGroupMatcher;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class represents the microeconomic environment of a food addiction experiment.
 */
public abstract class FoodAddictionEnvironment extends Environment implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = 3573337621694528692L;

    /** Number of subjects */
    private static final int NUMBER_OF_SUBJECTS = 1;

    /** The roles */
    private static final String ROLE_PARTICIPANT = "Participant";

    /** Number of images */
    public static final int NUMBER_OF_FOOD_IMAGES = 30;
    public static final int NUMBER_OF_NEUTRAL_IMAGES = 12;
    public static final int NUMBER_OF_TRIAL_IMAGES = 4;

    /** Timing */
    public static final long TIME_PERIOD = 1000L; // 1 second
    public static final long INFO_TIME = 4 * 1000L; // 4 seconds specified by Janelle
    public static final long REST_TIME = 5 * 60 * 1000L; // 5 minutes
    public static final long DISPLAY_TIME = 7 * 1000L;
    public static final long DISPLAY_TIME_TRIAL = 5 * 1000L;
    public static final long FIXATION_TIME = 3 * 1000L;
    public static final long DELAY_TIME = 0L;
    public static final ArrayList<Long> NEXT_PROMPT_TIMES = new ArrayList<Long>(){{add(100 * 1000L); add(200 * 1000L);}};
    public static final long DELAY_AFTER_PROMPT = 1000L; // 1 second

    /** Steering task settings */
    public static final int w = 50; // TODO adjust this value ?
    public static final int D = 400; // TODO adjust this value ?
    public static final int RECORDING_PERIOD = 1; // Cursor location sampled every x milliseconds.

    /** UI settings lifted from AuctionFever */
    public static final Color COLOR_SCREENBACKGROUND = new Color(197, 217, 240);
    public static final Color COLOR_BORDER = new Color(28, 68, 120);
    public static final Color COLOR_FONT = Color.BLACK;
    public static final Color COLOR_BOXBACKGROUND = Color.WHITE;
    public static final String FONT_FONTFAMLY = "Arial";
    public static final Font FONT_BOX = new Font(FONT_FONTFAMLY, Font.BOLD, 32);
    public static final Font FONT_TITLE = new Font(FONT_FONTFAMLY, Font.BOLD, 44);
    public static final Font FONT_BUTTON = new Font(FONT_FONTFAMLY, Font.PLAIN, 20);

    /** File paths */
    protected static final String[] JUNK_FOOD_PATHS = new String[] {"resources/junk/Savoury_18.jpg", "resources/junk/Savoury_28.jpg", "resources/junk/Savoury_36.jpg", "resources/junk/Savoury_46.jpg", "resources/junk/Savoury_50.jpg", "resources/junk/Savoury_59.jpg", "resources/junk/Savoury_82.jpg", "resources/junk/Savoury_86.jpg", "resources/junk/Savoury_101.jpg", "resources/junk/Savoury_116.jpg", "resources/junk/Savoury_124.jpg", "resources/junk/Savoury_142.jpg", "resources/junk/Savoury_166.jpg", "resources/junk/Savoury_174.jpg", "resources/junk/Savoury_199.jpg",
                                                                    "resources/junk/Sweet_20.jpg", "resources/junk/Sweet_29.jpg", "resources/junk/Sweet_32.jpg", "resources/junk/Sweet_38.jpg", "resources/junk/Sweet_44.jpg", "resources/junk/Sweet_62.jpg", "resources/junk/Sweet_84.jpg", "resources/junk/Sweet_100.jpg", "resources/junk/Sweet_109.jpg", "resources/junk/Sweet_118.jpg", "resources/junk/Sweet_127.jpg", "resources/junk/Sweet_130.jpg", "resources/junk/Sweet_160.jpg", "resources/junk/Sweet_164.jpg", "resources/junk/Sweet_187.jpg"};
    protected static final String[] HEALTHY_FOOD_PATHS = new String[] {"resources/healthy/Dairy_15.jpg", "resources/healthy/Dairy_23.jpg", "resources/healthy/Dairy_67.jpg", "resources/healthy/Dairy_72.jpg", "resources/healthy/Dairy_85.jpg", "resources/healthy/Dairy_99.jpg",
                                                                       "resources/healthy/Fruit_30.jpg", "resources/healthy/Fruit_64.jpg", "resources/healthy/Fruit_74.jpg", "resources/healthy/Fruit_93.jpg", "resources/healthy/Fruit_123.jpg", "resources/healthy/Fruit_132.jpg",
                                                                       "resources/healthy/Grain_27.jpg", "resources/healthy/Grain_40.jpg", "resources/healthy/Grain_71.jpg", "resources/healthy/Grain_115.jpg", "resources/healthy/Grain_120.jpg", "resources/healthy/Grain_163.jpg",
                                                                       "resources/healthy/Meat_21.jpg", "resources/healthy/Meat_45.jpg", "resources/healthy/Meat_134.jpg", "resources/healthy/Meat_156.jpg", "resources/healthy/Meat_167.jpg", "resources/healthy/Meat_198.jpg",
                                                                       "resources/healthy/Veg_136.jpg", "resources/healthy/Veg_150.jpg", "resources/healthy/Veg_159.jpg", "resources/healthy/Veg_170.jpg", "resources/healthy/Veg_178.jpg", "resources/healthy/Veg_192.jpg"};
    public static final String[] NEUTRAL_PATHS = new String[] {"resources/neutral/Neutral_1.jpg", "resources/neutral/Neutral_2.jpg", "resources/neutral/Neutral_3.jpg", "resources/neutral/Neutral_4.jpg", "resources/neutral/Neutral_5.jpg", "resources/neutral/Neutral_6.jpg", "resources/neutral/Neutral_7.jpg", "resources/neutral/Neutral_8.jpg", "resources/neutral/Neutral_9.jpg", "resources/neutral/Neutral_10.jpg", "resources/neutral/Neutral_11.jpg", "resources/neutral/Neutral_12.jpg", "resources/neutral/Neutral_13.jpg", "resources/neutral/Neutral_14.jpg", "resources/neutral/Neutral_15.jpg"};
    public static final String[] TRIAL_1_PATHS = new String[] {"resources/trial/Practice_trial_1.jpg", "resources/trial/Practice_trial_2.jpg", "resources/trial/Practice_trial_3.jpg", "resources/trial/Practice_trial_4.jpg"};
    public static final String[] TRIAL_2_PATHS = new String[] {"resources/trial/Practice_trial_5.jpg", "resources/trial/Practice_trial_6.jpg", "resources/trial/Practice_trial_7.jpg", "resources/trial/Practice_trial_8.jpg"};

    /** Text */
    public static final String TEXT_DEFAULT = "Please wait for the experiment to continue...";

    /**
     * This constructor instantiates a new food addiction environment.
     * It adds roles, a roleMatcher and a subjectGroupMatcher.
     */
    public FoodAddictionEnvironment() {

        super();

        // Add role
        this.roles.add(ROLE_PARTICIPANT);

        // Set role matcher
        this.roleMatcher = new FoodAddictionRoleMatcher(roles);

        // Set subject group matcher
        this.subjectGroupMatcher = new SingleRoleGroupMatcher(roles);

        this.setResetMatchersAfterTreatmentBlocks(false);
    }

    @Override
    public RoleMatcher getRoleMatcher() {
        return roleMatcher;
    }

    @Override
    public SubjectGroupMatcher getSubjectGroupMatcher() {
        return subjectGroupMatcher;
    }

    /** Shared UI elements */

    public static JPanel createInfoTextBox(String text) {
        JPanel infoPanel = new JPanel();
        infoPanel.setBackground(COLOR_BOXBACKGROUND);
        infoPanel.setLayout(new BorderLayout(0, 0));
        infoPanel.setBorder(BorderFactory.createLineBorder(COLOR_BORDER, 3));
        infoPanel.setPreferredSize(new Dimension(900, 150));
        infoPanel.setMaximumSize(infoPanel.getPreferredSize());
        infoPanel.setMinimumSize(infoPanel.getPreferredSize());

        JLabel labelInfo = new JLabel("<html><center>" + text + "</center></html>", SwingConstants.CENTER);
        labelInfo.setFont(FONT_BOX);
        labelInfo.setForeground(COLOR_FONT);

        infoPanel.add(labelInfo);

        return infoPanel;
    }

    public static Button createButton(String text) {
        Button returnButton = new Button(text);
        returnButton.setFont(FONT_BUTTON);
        returnButton.setPreferredSize(new Dimension(200, 50));
        returnButton.setMaximumSize(returnButton.getPreferredSize());
        returnButton.setMinimumSize(returnButton.getPreferredSize());
        return returnButton;
    }

    /** Environment dependent */

    // Image paths
    public abstract String[] getInterval1ImagePaths();
    public abstract String[] getInterval2ImagePaths();
}
