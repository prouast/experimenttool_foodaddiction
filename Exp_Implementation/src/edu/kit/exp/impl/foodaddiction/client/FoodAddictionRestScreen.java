package edu.kit.exp.impl.foodaddiction.client;

import java.awt.*;
import javax.swing.*;

public class FoodAddictionRestScreen extends FoodAddictionScreen {

    /** serialVersionUID */
    private static final long serialVersionUID = -96969725593420021L;

    private final Color COLOR_CROSS = Color.BLACK;
    private final int BAR_THICKNESS = 33;
    private final int BAR_LENGTH = 100;

    /**
     * This constructor creates am instance of FoodAddictionRestScreen.
     * @param gameId The String ID of the running game, whose institution triggered that screen to be shown at the client.
     * @param parameter A ParamObject containing parameters used in this screen.
     * @param screenId A String which contains the global screen ID that has to be given for a complete trial entry at server side.
     * @param showUpTime A Long variable which contains the time the screen will be shown to a client.
     */
    public FoodAddictionRestScreen(String gameId, ParamObject parameter, String screenId, Long showUpTime) {

        super(gameId, parameter, screenId, showUpTime);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        add(Box.createVerticalGlue());

        JPanel barTop = new JPanel();
        barTop.setBackground(COLOR_CROSS);
        barTop.setPreferredSize(new Dimension(BAR_THICKNESS, BAR_LENGTH));
        barTop.setMaximumSize(barTop.getPreferredSize());
        barTop.setMinimumSize(barTop.getPreferredSize());

        JPanel barMiddle = new JPanel();
        barMiddle.setBackground(COLOR_CROSS);
        barMiddle.setPreferredSize(new Dimension((2 * BAR_LENGTH) + BAR_THICKNESS, BAR_THICKNESS));
        barMiddle.setMaximumSize(barMiddle.getPreferredSize());
        barMiddle.setMinimumSize(barMiddle.getPreferredSize());

        JPanel barBottom = new JPanel();
        barBottom.setBackground(COLOR_CROSS);
        barBottom.setPreferredSize(new Dimension(BAR_THICKNESS, BAR_LENGTH));
        barBottom.setMaximumSize(barBottom.getPreferredSize());
        barBottom.setMinimumSize(barBottom.getPreferredSize());

        add(barTop);
        add(barMiddle);
        add(barBottom);

        add(Box.createVerticalGlue());
    }
}
