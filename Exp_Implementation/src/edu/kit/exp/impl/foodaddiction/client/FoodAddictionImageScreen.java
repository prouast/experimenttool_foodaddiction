package edu.kit.exp.impl.foodaddiction.client;

import edu.kit.exp.common.LogHandler;
import edu.kit.exp.impl.foodaddiction.server.FoodAddictionEnvironment;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.Timer;

/**
 * Provides a screen for displaying images.
 */
public class FoodAddictionImageScreen extends FoodAddictionScreen {

    /** serialVersionUID */
    private static final long serialVersionUID = -6529444018073060146L;

    /** Possible log events */
    public enum LogValType {
        TRIAL_1,
        TRIAL_2,
        INTERVAL_1,
        WASH_OUT,
        INTERVAL_2
    }

    /** Possible states */
    private enum State {
        IDLE,
        DISPLAY,
        FIXATION
    }

    /** The current state */
    private State state;

    /** Timing */
    private Timer timer;
    private long timePassedSinceStateChange; // Time passed since last state change
    private long timePassed; // Time passed in total
    private ArrayList<Long> promptTimes;
    private long displayTime;
    private long fixationTime;

    /** The image paths */
    private String[] images;
    private int currentImage;

    /** The panel */
    private JPanel panel;

    /** The next button */
    private Button button;
    private JPanel buttonInfoPanel;

    /** Dimensions */
    private final int IMAGE_WIDTH = 826;
    private final int IMAGE_HEIGHT = 620;
    private final Color COLOR_CROSS = Color.BLACK;
    private final int BAR_THICKNESS = 33;
    private final int BAR_LENGTH = 100;

    /**
     * Defines the parameters that can be provided for this screen.
     */
    public static class ParamObject extends FoodAddictionScreen.ParamObject {

        /** serialVersionUID */
        private static final long serialVersionUID = 8587338133246400242L;

        /** The log val */
        private LogValType logVal;

        /** The image path. */
        private String[] imagePaths;

        /** Display options */
        private int numberOfImages;
        private long displayTime;
        private long fixationTime;
        private ArrayList<Long> nextPromptTimes;

        public LogValType getLogVal() {
            return logVal;
        }
        public String[] getImagePaths() {
            return imagePaths;
        }
        public int getNumberOfImages() {
            return numberOfImages;
        }
        public long getDisplayTime() {
            return displayTime;
        }
        public long getFixationTime() {
            return fixationTime;
        }
        public ArrayList<Long> getNextPromptTimes() {
            return nextPromptTimes;
        }

        /**
         * This constructor creates an instance of ParamObject.
         */
        public ParamObject() {
            super();
        }

        /**
         * This constructor creates an instance of ParamObject.
         */
        public ParamObject(LogValType logVal, String[] imagePaths, int numberOfImages, long displayTime, long fixationTime, ArrayList<Long> nextPromptTimes) {
            this.logVal = logVal;
            this.imagePaths = imagePaths;
            this.numberOfImages = numberOfImages;
            this.displayTime = displayTime;
            this.fixationTime = fixationTime;
            this.nextPromptTimes = nextPromptTimes;
        }
    }

    /**
     * This constructor creates an instance of FoodAddictionImageScreen.
     * @param gameId
     * @param parameter
     * @param screenId
     * @param showUpTime
     */
    public FoodAddictionImageScreen(String gameId, ParamObject parameter, String screenId, Long showUpTime) {
        super(gameId, parameter, screenId, showUpTime);

        // Load image paths for display
        assert parameter.getImagePaths().length == parameter.getNumberOfImages();
        images = new String[parameter.getNumberOfImages()];
        System.arraycopy(parameter.getImagePaths(), 0, images, 0, parameter.getNumberOfImages());

        // Set up image panel
        panel = new JPanel();
        panel.setMaximumSize(new Dimension(IMAGE_WIDTH, IMAGE_HEIGHT));
        panel.setMinimumSize(new Dimension(IMAGE_WIDTH, IMAGE_HEIGHT));
        panel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up next button
        button = FoodAddictionEnvironment.createButton("Next");
        button.setVisible(false);
        button.setEnabled(false);
        button.addActionListener(e -> {
            timer = new Timer();
            timer.scheduleAtFixedRate(new FoodAddictionImageScreenTimerTask(),
                    FoodAddictionEnvironment.DELAY_AFTER_PROMPT, FoodAddictionEnvironment.TIME_PERIOD); // Restart timer
            guiController.sendTrialLogMessage(getGameId(),
                    "EVENT_CLICK_NEXT_BUTTON", getScreenId(), "", new Date().getTime());
            button.setEnabled(false);
            button.setVisible(false);
            buttonInfoPanel.setVisible(false);
            repaint();
            revalidate();
        });

        // Set up button info panel
        buttonInfoPanel = FoodAddictionEnvironment.createInfoTextBox("Please click 'Next' to continue.");
        buttonInfoPanel.setVisible(false);

        // Create timer
        timer = new Timer();
        timePassedSinceStateChange = 0;
        timePassed = 0;
        promptTimes = parameter.getNextPromptTimes();
        displayTime = parameter.getDisplayTime();
        fixationTime = parameter.getFixationTime();
        state = State.IDLE;
        timer.scheduleAtFixedRate(new FoodAddictionImageScreenTimerTask(),
                FoodAddictionEnvironment.DELAY_TIME, FoodAddictionEnvironment.TIME_PERIOD);

        // Add elements
        add(Box.createVerticalGlue());
        add(Box.createRigidArea(new Dimension(0, 50)));
        add(panel);
        add(Box.createRigidArea(new Dimension(0, 25)));
        add(buttonInfoPanel);
        add(Box.createRigidArea(new Dimension(0, 25)));
        add(button);
        add(Box.createVerticalGlue());
    }

    /**
     * Displays an image on imagePanel
     */
    private void displayImage() {
        LogHandler.printInfo("Display image " + images[currentImage]);
        ImageIcon image = new ImageIcon(getClass().getResource(images[currentImage++]));
        JLabel label = new JLabel("", image, JLabel.CENTER);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.removeAll();
        panel.setBackground(Color.WHITE);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setMaximumSize(new Dimension(IMAGE_WIDTH, IMAGE_HEIGHT));
        panel.setMinimumSize(new Dimension(IMAGE_WIDTH, IMAGE_HEIGHT));
        panel.add(Box.createVerticalGlue());
        panel.add(label, BorderLayout.CENTER);
        panel.add(Box.createVerticalGlue());
        repaint();
        revalidate();
    }

    /**
     * Displays the cross.
     */
    private void displayCross() {
        JPanel barTop = new JPanel();
        barTop.setBackground(COLOR_CROSS);
        barTop.setPreferredSize(new Dimension(BAR_THICKNESS, BAR_LENGTH));
        barTop.setMaximumSize(barTop.getPreferredSize());
        barTop.setMinimumSize(barTop.getPreferredSize());
        JPanel barMiddle = new JPanel();
        barMiddle.setBackground(COLOR_CROSS);
        barMiddle.setPreferredSize(new Dimension((2 * BAR_LENGTH) + BAR_THICKNESS, BAR_THICKNESS));
        barMiddle.setMaximumSize(barMiddle.getPreferredSize());
        barMiddle.setMinimumSize(barMiddle.getPreferredSize());
        JPanel barBottom = new JPanel();
        barBottom.setBackground(COLOR_CROSS);
        barBottom.setPreferredSize(new Dimension(BAR_THICKNESS, BAR_LENGTH));
        barBottom.setMaximumSize(barBottom.getPreferredSize());
        barBottom.setMinimumSize(barBottom.getPreferredSize());
        panel.removeAll();
        panel.setBackground(FoodAddictionEnvironment.COLOR_SCREENBACKGROUND);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setMaximumSize(new Dimension(IMAGE_WIDTH, IMAGE_HEIGHT));
        panel.setMinimumSize(new Dimension(IMAGE_WIDTH, IMAGE_HEIGHT));
        panel.add(Box.createVerticalGlue());
        panel.add(barTop);
        panel.add(barMiddle);
        panel.add(barBottom);
        panel.add(Box.createVerticalGlue());
        repaint();
        revalidate();
    }

    /**
     * This timer task handles the sequence of events
     */
    private class FoodAddictionImageScreenTimerTask extends TimerTask {

        @Override
        public void run() {
            // If necessary, display next button
            if (promptTimes.contains(timePassed)) {
                LogHandler.printInfo("Prompt time " + timePassed + " reached.");
                guiController.sendTrialLogMessage(getGameId(), "EVENT_SHOW_NEXT_BUTTON",
                        getScreenId(), "", new Date().getTime());
                button.setEnabled(true);
                button.setVisible(true);
                buttonInfoPanel.setVisible(true);
                repaint();
                revalidate();
                state = State.IDLE;
                timer.cancel();
            } else {
                // If necessary, change state
                switch (state) {
                    case IDLE:
                        guiController.sendTrialLogMessage(getGameId(), "EVENT_SHOW_IMAGE_START",
                                getScreenId(), images[currentImage], new Date().getTime());
                        displayImage();
                        state = State.DISPLAY;
                        timePassedSinceStateChange = 0;
                        break;
                    case DISPLAY:
                        if (timePassedSinceStateChange % displayTime == 0) {
                            guiController.sendTrialLogMessage(getGameId(), "EVENT_SHOW_IMAGE_END",
                                    getScreenId(), images[currentImage-1], new Date().getTime());
                            if (currentImage == images.length) {
                                LogHandler.printInfo("Finished");
                                timer.cancel();
                                guiController.sendClientResponseMessageAndWait(getParameter(), getGameId(),
                                        getScreenId());
                            } else {
                                guiController.sendTrialLogMessage(getGameId(), "EVENT_FIXATION_START",
                                        getScreenId(), "", new Date().getTime());
                                displayCross();
                                state = State.FIXATION;
                                timePassedSinceStateChange = 0;
                            }
                        }
                        break;
                    case FIXATION:
                        if (timePassedSinceStateChange % fixationTime == 0) {
                            guiController.sendTrialLogMessage(getGameId(), "EVENT_FIXATION_END",
                                    getScreenId(), "", new Date().getTime());
                            guiController.sendTrialLogMessage(getGameId(), "EVENT_SHOW_IMAGE_START",
                                    getScreenId(), images[currentImage], new Date().getTime());
                            displayImage();
                            state = State.DISPLAY;
                            timePassedSinceStateChange = 0;
                        }
                        break;
                    default:
                        break;
                }
            }

            timePassedSinceStateChange = timePassedSinceStateChange + FoodAddictionEnvironment.TIME_PERIOD;
            timePassed = timePassed + FoodAddictionEnvironment.TIME_PERIOD;
        }
    }
}
