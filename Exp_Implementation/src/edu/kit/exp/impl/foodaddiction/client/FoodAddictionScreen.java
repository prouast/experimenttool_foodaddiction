package edu.kit.exp.impl.foodaddiction.client;

import edu.kit.exp.client.gui.screens.Screen;
import edu.kit.exp.impl.foodaddiction.server.FoodAddictionEnvironment;

import javax.swing.*;
import java.awt.*;

/**
 * Generic screen class for the food addiction experiment
 * Sets background and border
 */
public class FoodAddictionScreen extends Screen {

    /** serialVersionUID */
    private static final long serialVersionUID = -4179132269895373022L;

    /**
     * Defines the parameters that can be provided for this screen.
     */
    public static class ParamObject extends Screen.ParamObject {

        /** serialVersionUID */
        private static final long serialVersionUID = 7131579767546124807L;

        /**
         * This constructor creates and instance of ParamObject.
         */
        public ParamObject() {

        }
    }

    public static class ResponseObject extends Screen.ResponseObject {
        private static final long serialVersionUID = 3037559331071259292L;
    }

    /**
     * This constructor creates an instance of FoodAddictionScreen.
     * @param gameId The String ID of the running game, whose institution triggered that screen to be shown at the client.
     * @param parameter A ParamObject containing parameters used in this screen.
     * @param screenId A String which contains the global screen ID that has to be given for a complete trial entry at server side.
     * @param showUpTime A Long variable which contains the time the screen will be shown to a client.
     */
    public FoodAddictionScreen(String gameId, ParamObject parameter, String screenId, Long showUpTime) {
        super(gameId, parameter, screenId, showUpTime);

        setLogDisplayEvent(true);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBackground(FoodAddictionEnvironment.COLOR_SCREENBACKGROUND);
        setBorder(BorderFactory.createLineBorder(FoodAddictionEnvironment.COLOR_BOXBACKGROUND, 10));
    }
}
