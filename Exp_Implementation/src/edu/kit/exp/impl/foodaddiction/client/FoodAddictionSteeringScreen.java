package edu.kit.exp.impl.foodaddiction.client;

import edu.kit.exp.common.LogHandler;
import edu.kit.exp.impl.foodaddiction.server.FoodAddictionEnvironment;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.Timer;

/**
 * Provides a screen for the steering task.
 */
public class FoodAddictionSteeringScreen extends FoodAddictionScreen {

    /** serialVersionUID */
    private static final long serialVersionUID = 7423873997247959594L;

    /** Possible log events */
    public enum LogValType {
        TRIAL,
        INTERVAL_1,
        INTERVAL_2
    }

    /** UI elements */
    private JPanel panel;
    private Button beginButton;
    private Button finishButton;
    private JPanel taskPanel;
    private LinesComponent linesComponent;

    /** The cursor coordinates */
    private Point lastCursorLocation;
    private TreeMap<Long, Point> cursorLocations;

    /**
     * Defines the parameters that can be provided for this screen
     */
    public static class ParamObject extends FoodAddictionScreen.ParamObject {

        /** serialVersionUID */
        private static final long serialVersionUID = -5221931745490644092L;

        /** The log val */
        private LogValType logVal;

        /** Dimensions */
        private int w;
        private int D;

        public LogValType getLogVal() {
            return logVal;
        }
        public int getW() {
            return w;
        }
        public int getD() {
            return D;
        }

        /**
         * This constructor creates an instance of ParamObject.
         */
        public ParamObject() {
            super();
        }

        /**
         * This constructor creates an instance of ParamObject.
         */
        public ParamObject(LogValType logVal, int w, int D) {
            this.logVal = logVal;
            this.w = w;
            this.D = D;
        }
    }

    /**
     * This constructor creates an instance of FoodAddictionSteeringScreen
     * @param gameId
     * @param parameter
     * @param screenId
     * @param showUpTime
     */
    public FoodAddictionSteeringScreen(String gameId, ParamObject parameter, String screenId, Long showUpTime) {
        super(gameId, parameter, screenId, showUpTime);

        LogHandler.printInfo("In steering screen.");

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up timer
        Timer timer = new Timer();

        // Set up main panel
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.setAlignmentY(Component.CENTER_ALIGNMENT);
        panel.setBackground(FoodAddictionEnvironment.COLOR_SCREENBACKGROUND);

        // Set up buttons
        beginButton = FoodAddictionEnvironment.createButton("Begin");
        beginButton.setEnabled(true);
        beginButton.addActionListener(e -> {
            beginButton.setEnabled(false);
            finishButton.setEnabled(true);
            guiController.sendTrialLogMessage(getGameId(), "EVENT_CLICK_BEGIN",
                    getScreenId(), "", new Date().getTime());
            cursorLocations = new TreeMap<>();
            Point cursorLocation = MouseInfo.getPointerInfo().getLocation();
            lastCursorLocation = new Point(cursorLocation.x - taskPanel.getLocationOnScreen().x,
                                           cursorLocation.y - taskPanel.getLocationOnScreen().y);
            timer.scheduleAtFixedRate(new FoodAddictionSteeringScreenTimerTask(),
                    0, FoodAddictionEnvironment.RECORDING_PERIOD);
        });
        finishButton = FoodAddictionEnvironment.createButton("Finish");
        finishButton.setEnabled(false);
        finishButton.addActionListener(e -> {
            timer.cancel();
            finishButton.setEnabled(false);
            guiController.sendTrialLogMessage(getGameId(), "EVENT_CLICK_FINISH",
                    getScreenId(), "", new Date().getTime());
            FileWriter fileWriter;
            try {
                fileWriter = new FileWriter( new Date().getTime() + "_cursorLocations.csv");
                fileWriter.append("date;x;y\n");
                SortedSet<Long> keys = new TreeSet<>(cursorLocations.keySet());
                for (Long key: keys) {
                    fileWriter.append(key.toString());
                    fileWriter.append(";");
                    fileWriter.append(Integer.toString(cursorLocations.get(key).x));
                    fileWriter.append(";");
                    fileWriter.append(Integer.toString(cursorLocations.get(key).y));
                    fileWriter.append("\n");
                }
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            guiController.sendClientResponseMessageAndWait(getParameter(), getGameId(), getScreenId());
        });

        // Set up panel
        taskPanel = new JPanel();
        taskPanel.setBackground(FoodAddictionEnvironment.COLOR_SCREENBACKGROUND);
        taskPanel.setPreferredSize(new Dimension(parameter.getD(),3 * parameter.getW()));
        taskPanel.setMinimumSize(taskPanel.getPreferredSize());
        taskPanel.setMaximumSize(taskPanel.getPreferredSize());
        taskPanel.setLayout(new BoxLayout(taskPanel, BoxLayout.Y_AXIS));
        taskPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        JPanel barTop = new JPanel();
        barTop.setBackground(FoodAddictionEnvironment.COLOR_BORDER);
        barTop.setPreferredSize(new Dimension(parameter.getD(), parameter.getW()));
        barTop.setMaximumSize(barTop.getPreferredSize());
        barTop.setMinimumSize(barTop.getPreferredSize());
        linesComponent = new LinesComponent(parameter.getD(), parameter.getW());
        JPanel barBottom = new JPanel();
        barBottom.setBackground(FoodAddictionEnvironment.COLOR_BORDER);
        barBottom.setPreferredSize(new Dimension(parameter.getD(), parameter.getW()));
        barBottom.setMaximumSize(barBottom.getPreferredSize());
        barBottom.setMinimumSize(barBottom.getPreferredSize());
        taskPanel.add(barTop);
        taskPanel.add(linesComponent);
        taskPanel.add(barBottom);

        // Add components to main panel
        panel.add(Box.createHorizontalGlue());
        panel.add(beginButton);
        panel.add(Box.createRigidArea(new Dimension(50,0)));
        panel.add(taskPanel);
        panel.add(Box.createRigidArea(new Dimension(50,0)));
        panel.add(finishButton);
        panel.add(Box.createHorizontalGlue());

        // Add components
        add(Box.createVerticalGlue());
        add(FoodAddictionEnvironment.createInfoTextBox("Using the mouse cursor please draw a line through the tunnel displayed below, as quickly and as horizontal as possible."));
        add(Box.createVerticalGlue());
        add(panel);
        add(Box.createVerticalGlue());

        validate();
        repaint();
    }

    /**
     * Component that can be painted with lines
     */
    private static class LinesComponent extends JComponent {

        private ArrayList<Line2D.Double> lines;

        LinesComponent(int width, int height) {
            super();
            setPreferredSize(new Dimension(width, height));
            lines = new ArrayList<>();
        }

        @Override
        protected void paintComponent(Graphics g) {
            g.setColor(FoodAddictionEnvironment.COLOR_SCREENBACKGROUND);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.BLACK);
            for (Line2D.Double line : lines) {
                g.drawLine(
                        transformX(line.getX1()),
                        transformY(line.getY1()),
                        transformX(line.getX2()),
                        transformY(line.getY2())
                );
            }
        }

        public void addLine(Point p1, Point p2) {
            lines.add(new Line2D.Double(p1.x, p1.y, p2.x, p2.y));
            repaint();
        }

        private int transformX(double x) {
            return Math.min(Math.max(((int)x), 0), getWidth());
        }
        private int transformY(double y) {
            return Math.min(Math.max((int)(y - getHeight()), 0), getHeight());
        }
    }

    /**
     * This timerTask coordinates the recording of the cursor location
     */
    private class FoodAddictionSteeringScreenTimerTask extends TimerTask {
        @Override
        public void run() {
            // Record cursor location relative to panel
            Point cursorLocationScreen = MouseInfo.getPointerInfo().getLocation();
            Point cursorLocation = new Point(cursorLocationScreen.x - taskPanel.getLocationOnScreen().x,
                                             cursorLocationScreen.y - taskPanel.getLocationOnScreen().y);
            cursorLocations.put(new Date().getTime(), cursorLocation);
            // Draw cursor location
            linesComponent.addLine(lastCursorLocation, cursorLocation);
            lastCursorLocation = cursorLocation;
            validate();
            repaint();
            FoodAddictionSteeringScreen.this.validate();
            FoodAddictionSteeringScreen.this.repaint();
        }
    }
}
