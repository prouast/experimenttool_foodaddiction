package edu.kit.exp.impl.foodaddiction.client;

import edu.kit.exp.client.gui.ClientGuiController;
import edu.kit.exp.impl.foodaddiction.server.FoodAddictionEnvironment;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;

/**
 * Provides a generic screen for displaying information.
 */
public class FoodAddictionInfoScreen extends FoodAddictionScreen {

    /** serialVersionUID */
    private static final long serialVersionUID = 4312251183745956993L;

    /** Possible log events */
    public enum LogValType {
        WELCOME_START,
        INITIAL_REST_INFO_START,
        INITIAL_REST_INFO_END,
        TRIAL_1_INFO_START,
        STEERING_TRIAL_INFO_START,
        TRIAL_2_INFO_START,
        INTERVAL_1_INFO_START,
        STEERING_1_INFO_START,
        WASH_OUT_INFO_START,
        INTERVAL_2_INFO_START,
        STEERING_2_INFO_START
    }

    /**
     * Defines the paramters that can be provided for this screen
     */
    public static class ParamObject extends FoodAddictionScreen.ParamObject {

        /** serialVersionUID */
        private static final long serialVersionUID = -8772935847684838761L;

        private LogValType logVal;
        private boolean showNextButton;
        private long timeToShowNextButton;
        private ArrayList<String> texts;

        public LogValType getLogVal() {
            return logVal;
        }
        public void setLogVals(LogValType logVals) {
            this.logVal = logVal;
        }
        public boolean getShowNextButton() {
            return showNextButton;
        }
        public void setShowNextButton(boolean showNextButton) {
            this.showNextButton = showNextButton;
        }
        public ArrayList<String> getTexts() {
            return texts;
        }
        public void setTexts(ArrayList<String> texts) {
            this.texts = texts;
        }
        public long getTimeToShowNextButton() {
            return timeToShowNextButton;
        }
        public void setTimeToShowNextButton(long timeToShowNextButton) {
            this.timeToShowNextButton = timeToShowNextButton;
        }

        /**
         * This constructor creates an instance of ParamObject.
         */
        public ParamObject(){
            super();
        }

        /**
         * This constructor creates an instance of ParamObject.
         * @param logVal What this instance should be logged as
         * @param texts What information should be displayed
         */
        public ParamObject(LogValType logVal, ArrayList<String> texts) {
            this(logVal, false, texts);
        }

        /**
         * This constructor creates an instance of ParamObject.
         * @param logVal What this instance should be logged as
         * @param showNextButton Whether a 'next' button should be displayed
         * @param texts What information should be displayed
         */
        public ParamObject(LogValType logVal, boolean showNextButton, ArrayList<String> texts) {
            this(logVal, showNextButton, 0L, texts);
        }

        /**
         * This constructor creates an instance of ParamObject.
         * @param logVal What this instance should be logged as
         * @param showNextButton Whether a 'next' button should be displayed
         * @param timeToShowNextButton
         * @param texts What information should be displayed
         */
        public ParamObject(LogValType logVal, boolean showNextButton, long timeToShowNextButton, ArrayList<String> texts) {
            this.setLogValueEnter(logVal.name());
            this.setLogValueExit(logVal.name());
            this.texts = texts;
            this.logVal = logVal;
            this.showNextButton = showNextButton;
            this.timeToShowNextButton = timeToShowNextButton;
        }
    }

    /**
     * This constructor creates am instance of FoodAddictionInfoScreen.
     * @param gameId The String ID of the running game, whose institution triggered that screen to be shown at the client.
     * @param parameter A ParamObject containing parameters used in this screen.
     * @param screenId A String which contains the global screen ID that has to be given for a complete trial entry at server side.
     * @param showUpTime A Long variable which contains the time the screen will be shown to a client.
     */
    public FoodAddictionInfoScreen(String gameId, ParamObject parameter, String screenId, Long showUpTime) {
        super(gameId, parameter, screenId, showUpTime);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        add(Box.createVerticalGlue());

        // Add texts
        if (parameter == null || parameter.getTexts().size() == 0) {
            add(FoodAddictionEnvironment.createInfoTextBox(FoodAddictionEnvironment.TEXT_DEFAULT));
        } else {
            for (int i = 0; i < parameter.getTexts().size(); i++) {
                if (i != 0) {
                    add(Box.createRigidArea(new Dimension(0, 50)));
                }
                add(FoodAddictionEnvironment.createInfoTextBox(parameter.getTexts().get(i).toString()));
            }
        }

        if (parameter.getShowNextButton()) {
            add(Box.createRigidArea(new Dimension(0, 50)));
            Button buttonNext = FoodAddictionEnvironment.createButton("Next");
            buttonNext.setVisible(false);
            buttonNext.setEnabled(false);
            buttonNext.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    ((Button) arg0.getSource()).setVisible(false);
                    ClientGuiController.getInstance().sendTrialLogMessage(getGameId(), "EVENT_NEXT_CLICK", getScreenId(), "", new Date().getTime());
                    guiController.sendClientResponseMessageAndWait(getParameter(), getGameId(), getScreenId());
                }
            });
            add(buttonNext);

            java.util.Timer timerButtonNext = new java.util.Timer();
            timerButtonNext.schedule(new TimerTask() {
                @Override
                public void run() {
                    ClientGuiController.getInstance().sendTrialLogMessage(getGameId(), "EVENT_SHOW_BUTTON", getScreenId(), "", new Date().getTime());
                    buttonNext.setEnabled(true);
                    buttonNext.setVisible(true);
                    repaint();
                    revalidate();
                }
            }, parameter.getTimeToShowNextButton());
        }

        add(Box.createVerticalGlue());
    }
}























